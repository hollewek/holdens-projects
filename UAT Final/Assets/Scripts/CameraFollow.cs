﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	private GameObject player; // Variable for the player GameObject

	public float xMin;//
	public float xMax;/// Sets the parameters for 
	public float yMin;/// the camera follow function
	public float yMax;//

	// Use this for initialization
	void Start () { // Finds the player with its Player tag
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	// Update is called once per frame
	void LateUpdate () { // Camera follows the players position
		float x = Mathf.Clamp (player.transform.position.x, xMin, xMax);
		float y = Mathf.Clamp (player.transform.position.y, yMin, yMax);
		gameObject.transform.position = new Vector3 (x, y, gameObject.transform.position.z);
	}
}
