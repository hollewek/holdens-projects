﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayAgain : MonoBehaviour {

	public void PlayGame (){ //Allows the play buttons (Start and Game over Screens) to load the first level of the game
		SceneManager.LoadScene ("Level 1");
	}
}
