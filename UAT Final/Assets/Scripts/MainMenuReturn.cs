﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuReturn : MonoBehaviour {

	public void StartMenu (){ //Allows the play buttons (Start and Game over Screens) to load the first level of the game
		SceneManager.LoadScene ("Start Screen");
	}
	public void Quit(){
		Debug.Log ("quit game");
		Application.Quit ();
	}
}
