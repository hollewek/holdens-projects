﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour {

	public int counter = 0;
	public GameObject coin;

	void OnTriggerEnter2D () { //upon colliding with the player, the colletables will be removed from the game and add to the counter
		CoinCount.counter += 1;
		coin.SetActive (false);

	}
}