﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GemWin : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.tag == "Player"){
			SceneManager.LoadScene("Victory Screen");
		}
	}
}
