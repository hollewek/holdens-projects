﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float speed;// Speed
	public float jumpPower; // Jump Height
	private bool facingRight = false; // States if the player is facing left or right
	private float moveX; // Shows what direction the player is moving in
	public bool isGrounded; // Detects if the player is on the ground
	public int jumpCounter; //Counts number of jumps taken before landing
	public int jumpLimit = 2; // Sets maximum jumps in the air
	public Animator animator; // Creates a reference for animation

	// Update is called once per frame
	void Update () { //Constantly checks if the player is moving
		PlayerMove();
	}

	void PlayerMove() {
		// Controls
		moveX = Input.GetAxis("Horizontal");
		if (Input.GetButtonDown ("Jump") && jumpCounter < jumpLimit) {
			Jump ();
			jumpCounter++;
		}

		//Animations
		animator.SetFloat("Speed", Mathf.Abs(gameObject.GetComponent<Rigidbody2D>().velocity.x));

		//Player Direction
		if (moveX < 0.0f && facingRight == false)
			FlipPlayer();
		else if (moveX > 0.0f && facingRight == true)
			FlipPlayer();

		//physics
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (moveX * speed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
	}

	void Jump(){
		//Allows player to jump
		GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpPower);
		isGrounded = false;
		animator.SetBool ("IsJumping", true);

	}

	void FlipPlayer(){ //Adjusts the player sprite when moving in either left or right directions
		facingRight = !facingRight;
		Vector2 localScale = gameObject.transform.localScale;
		localScale.x *= -1;
		transform.localScale = localScale;
	}

	void OnCollisionEnter2D(Collision2D col){ //resets the player's jump counter upon landing on objects with Ground tag
		if (col.gameObject.tag == "Ground") {
			isGrounded = true;
			jumpCounter = 0;
			animator.SetBool ("IsJumping", false);
		}
	}
}