﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CoinCount : MonoBehaviour {

	public static int counter;
	public int internalCount;
	public GameObject coin1;//
	public GameObject coin2;//
	public GameObject coin3;// 
	public GameObject coin4;//
	public GameObject coin5;//
	public GameObject coin6;// Allows us to place our coin image on the script
	public GameObject coin7;//
	public GameObject coin8;// 
	public GameObject coin9;//
	public GameObject coin10;//
	public GameObject coin11;//
	public GameObject coin12;//

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		internalCount = counter;

		if (counter == 1) {
			coin1.SetActive(true);
		}
		if (counter == 2) {
			coin2.SetActive(true);
		}
		if (counter == 3) {
			coin3.SetActive(true);
		}
		if (counter == 4) {
			coin4.SetActive(true);
		}
		if (counter == 5) {
			coin5.SetActive(true);
		}
		if (counter == 6) {
			coin6.SetActive(true);
		}
		if (counter == 7) {
			coin7.SetActive(true);
		}
		if (counter == 8) {
			coin8.SetActive(true);
		}
		if (counter == 9) {
			coin9.SetActive(true);
		}
		if (counter == 10) {
			coin10.SetActive(true);
		}
		if (counter == 11) {
			coin11.SetActive(true);
		}
		if (counter == 12) {
			coin12.SetActive(true);
		}
		if (counter == 12) {
			SceneManager.LoadScene("Level 2");
			counter = 0; 	
		}

	}
}
