﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundryDestroy : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col) { //Destroys objects that do not have the Boundry tag
		if (col.gameObject.tag != "Player") {
			Destroy (col.gameObject);
		}

	}
} 