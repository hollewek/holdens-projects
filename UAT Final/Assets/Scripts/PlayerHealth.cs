﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

	public int health; //Determines number of lives before game over
	public bool hasDied; //States if the player is still alive
	private AudioSource tickSource; //Creates variable for audio generation
	public Vector3 startPosition; //Variable for player's start position


	// Use this for initialization
	void Start () {
		hasDied = false;
		tickSource = GetComponent<AudioSource> ();
		transform.position = startPosition; //captures the initial position on load
	}

	// Update is called once per frame
	void Update () { //kills the player if they fall off the map.
		if (gameObject.transform.position.y < -7) {
			Damage ();
		}
	}

	void OnCollisionEnter2D(Collision2D col){ //Detects if player makes contact with a hazard
		if (col.gameObject.tag == "Hazard") {
			Damage ();
		}
	}

	void Damage(){ //Kills the player if they make contact with an enemy
		health--;
		tickSource.Play ();

		if (health == 0) { // If health = 0 execute death function
			Die ();
		}

		gameObject.transform.position = startPosition; //Upon taking damage return player to start location.
	}

	void Die (){ //Sets scene to game over scene upon losing all lives
		SceneManager.LoadScene ("Game Over Screen"); 
	}
}